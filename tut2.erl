-module (tut2).
-export ([convert/2]).

convert(N, inch) ->
	N * 2.54;
convert(M, centimeter) ->
	M * 2.54.
